﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Linq;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace base16_parser
{
    class Program
    {
        private enum RunMode
        {
            FileMode,
            GitMode
        }

        static int Main(string[] args)
        {
            RunMode rm = RunMode.FileMode;
            string? path = null;

            if (args.Length > 0)
            {
                for (int a = 0; a < args.Length; a++)
                {
                    switch (args[a])
                    {
                        case "-g":
                        case "--git":
                            rm = RunMode.GitMode;
                            break;
                        case "-f":
                        case "--file":
                            rm = RunMode.FileMode;
                            break;
                        default:
                            path = args[a];
                            break;
                    }
                }
            }

            switch (rm)
            {
                case RunMode.FileMode:
                    path ??= Directory.GetCurrentDirectory();
                    return FileMode(path) ? 0 : 1;
                case RunMode.GitMode:
                    path ??= @"https://raw.githubusercontent.com/chriskempson/base16-schemes-source/master/list.yaml";
                    return GitMode(path) ? 0 : 1;
                default:
                    Console.WriteLine($"Invalid Run Mode: {rm}");
                    return 1;
            }
        }

        internal static Base16Scheme? ReadYaml(string file)
        {
            try
            {
                using var fileReader = File.OpenRead(file);
                using var reader = new StreamReader(fileReader);
                return new DeserializerBuilder()
                    .WithNamingConvention(CamelCaseNamingConvention.Instance)
                    .Build()
                    .Deserialize<Base16Scheme>(reader);
            }
            catch (System.Exception)
            {
                return null;
            }
        }

        internal static bool FileMode(string path)
        {
            var schemes = ProcessFiles(path);

            if (schemes == null) return false;

            WriteHaskell(schemes);

            return true;
        }

        internal static IEnumerable<Base16Scheme>? ProcessFiles(string path)
        {
            if (!Directory.Exists(path))
            {
                Console.WriteLine($"Invalid Directory: {path}");
                return null;
            }

            Console.WriteLine($"Processing Directory: {path}");

            List<Base16Scheme> schemes = new();
            foreach (var file in Directory.GetFiles(path, "*.yaml"))
            {
                var scheme = ReadYaml(file);
                if (scheme != null)
                    schemes.Add(scheme);
            }

            return schemes;
        }

        internal static bool GitMode(string url)
        {
            if (!Uri.TryCreate(url, UriKind.Absolute, out var listUri))
            {
                Console.WriteLine($"Invalid URL: {url}");
                return false;
            }

            try
            {
                Console.WriteLine($"Downloading List: {url}");
                using var wc = new System.Net.WebClient();
                using var s = wc.OpenRead(listUri);
                using var sr = new StreamReader(s);
                var deserializer =
                    new DeserializerBuilder()
                        .WithNamingConvention(CamelCaseNamingConvention.Instance)
                        .Build();

                var listObject = deserializer.Deserialize(sr);
                if (listObject is not null)
                {
                    Dictionary<string, Uri> listDict = new();
                    if (listObject is Dictionary<object, object> tempDict)
                    {
                        foreach (var repo in tempDict)
                        {
                            if (repo.Key is string scheme && Uri.TryCreate(repo.Value as string, UriKind.Absolute, out var repoUri))
                            {
                                listDict.Add(scheme, repoUri);
                            }
                        }
                    }

                    var parserTempPath = Path.Combine(Directory.GetCurrentDirectory(), "base16-parser");
                    if (Directory.Exists(parserTempPath)) Directory.Delete(parserTempPath, true);
                    Directory.CreateDirectory(parserTempPath);

                    List<Base16Scheme> schemes = new();
                    foreach (var repo in listDict)
                    {
                        Console.WriteLine($"Cloning Scheme: {repo.Key}");
                        var repoPath = Path.Combine(parserTempPath, repo.Key);
                        using var p = new Process()
                        {
                            StartInfo = new("git")
                            {
                                ArgumentList = {
                                    "clone",
                                    repo.Value.ToString(),
                                    repoPath
                                }
                            }
                        };
                        p.Start();
                        p.WaitForExit();

                        var repoSchemes = ProcessFiles(repoPath);
                        if (repoSchemes != null && repoSchemes.Any())
                            schemes.AddRange(repoSchemes);
                    }

                    if (Directory.Exists(parserTempPath)) Directory.Delete(parserTempPath, true);

                    WriteHaskell(schemes);
                }
            }
            catch (Exception ex)
            {
                PrintException(ex);
                return false;
            }

            return true;
        }

        internal static void WriteHaskell(IEnumerable<Base16Scheme> schemes)
        {
            if ((schemes?.Count() ?? 0) == 0) return;

            var outputFilePath = Path.Combine(Directory.GetCurrentDirectory(), "Base16Themes.hs");
            using var outputFile = File.Open(outputFilePath, System.IO.FileMode.Create, FileAccess.Write, FileShare.Read);
            using var sw = new StreamWriter(outputFile, System.Text.Encoding.UTF8);

            var organizedSchemes = from scheme in schemes
                                   orderby scheme.Scheme
                                   group scheme by scheme.HaskellInstanceName into groupedSchemes
                                   select groupedSchemes.FirstOrDefault();

            if (organizedSchemes != null && organizedSchemes.Count() > 0)
            {
                // This is the Base16 Haskell Module and Type
                sw.WriteLine("module Base16 ( Base16(..)");
                sw.WriteLine("              , " + string.Join(Environment.NewLine + "              , ", organizedSchemes.Select(s => s.HaskellInstanceName)));
                sw.WriteLine("              ) where");
                sw.WriteLine();
                sw.WriteLine("data Base16 = Base16 { scheme :: String");
                sw.WriteLine("                     , author :: String");
                sw.WriteLine("                     , base00 :: String");
                sw.WriteLine("                     , base01 :: String");
                sw.WriteLine("                     , base02 :: String");
                sw.WriteLine("                     , base03 :: String");
                sw.WriteLine("                     , base04 :: String");
                sw.WriteLine("                     , base05 :: String");
                sw.WriteLine("                     , base06 :: String");
                sw.WriteLine("                     , base07 :: String");
                sw.WriteLine("                     , base08 :: String");
                sw.WriteLine("                     , base09 :: String");
                sw.WriteLine("                     , base0A :: String");
                sw.WriteLine("                     , base0B :: String");
                sw.WriteLine("                     , base0C :: String");
                sw.WriteLine("                     , base0D :: String");
                sw.WriteLine("                     , base0E :: String");
                sw.WriteLine("                     , base0F :: String");
                sw.WriteLine("                     }");
                sw.WriteLine();

                foreach (var scheme in organizedSchemes)
                {
                    sw.WriteLine(scheme.GetHaskell());
                }
            }
        }

        internal static void PrintException(Exception ex)
        {
            Console.WriteLine("Exception:");
            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.StackTrace);
        }
    }
}
