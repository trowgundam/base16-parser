using System.Text;
using System.Text.RegularExpressions;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace base16_parser
{
    internal class Base16Scheme
    {   
        public string HaskellInstanceName => $"base16{Regex.Replace(this.Scheme, "[^a-zA-Z0-9]", "")}";
        public string Scheme { get; set; } = "Default Dark";
        public string Author { get; set; } = "Chris Kempson (http://chriskempson.com)";
        public string Base00 { get; set; } = "#181818";
        public string Base01 { get; set; } = "#282828";
        public string Base02 { get; set; } = "#383838";
        public string Base03 { get; set; } = "#585858";
        public string Base04 { get; set; } = "#b8b8b8";
        public string Base05 { get; set; } = "#d8d8d8";
        public string Base06 { get; set; } = "#e8e8e8";
        public string Base07 { get; set; } = "#f8f8f8";
        public string Base08 { get; set; } = "#ab4642";
        public string Base09 { get; set; } = "#dc9656";
        public string Base0A { get; set; } = "#f7ca88";
        public string Base0B { get; set; } = "#a1b56c";
        public string Base0C { get; set; } = "#86c1b9";
        public string Base0D { get; set; } = "#7cafc2";
        public string Base0E { get; set; } = "#ba8baf";
        public string Base0F { get; set; } = "#a16946";

        public override string ToString() =>
            new SerializerBuilder()
                .WithNamingConvention(CamelCaseNamingConvention.Instance)
                .Build()
                .Serialize(this);

        public string GetHaskell()
        {
            StringBuilder sb = new();
            sb.AppendLine($"{this.HaskellInstanceName} =");
            sb.AppendLine($"    Base16 {{ scheme = \"{this.Scheme}\"");
            sb.AppendLine($"           , author = \"{this.Author}\"");
            sb.AppendLine($"           , base00 = \"#{this.Base00}\"");
            sb.AppendLine($"           , base01 = \"#{this.Base01}\"");
            sb.AppendLine($"           , base02 = \"#{this.Base02}\"");
            sb.AppendLine($"           , base03 = \"#{this.Base03}\"");
            sb.AppendLine($"           , base04 = \"#{this.Base04}\"");
            sb.AppendLine($"           , base05 = \"#{this.Base05}\"");
            sb.AppendLine($"           , base06 = \"#{this.Base06}\"");
            sb.AppendLine($"           , base07 = \"#{this.Base07}\"");
            sb.AppendLine($"           , base08 = \"#{this.Base08}\"");
            sb.AppendLine($"           , base09 = \"#{this.Base09}\"");
            sb.AppendLine($"           , base0A = \"#{this.Base0A}\"");
            sb.AppendLine($"           , base0B = \"#{this.Base0B}\"");
            sb.AppendLine($"           , base0C = \"#{this.Base0C}\"");
            sb.AppendLine($"           , base0D = \"#{this.Base0D}\"");
            sb.AppendLine($"           , base0E = \"#{this.Base0E}\"");
            sb.AppendLine($"           , base0F = \"#{this.Base0F}\"");
            sb.AppendLine("           }");
            return sb.ToString();
        }
    }
}